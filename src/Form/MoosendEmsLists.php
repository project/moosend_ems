<?php

namespace Drupal\moosend_ems\Form;

use Drupal\moosend_ems\Service\MoosendEms;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MoosendEmsLists.
 *
 * Configuration form for enabling lists for use.
 * (ex: in either blocks or REST endpoints.)
 */
class MoosendEmsLists extends ConfigFormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Drupal\moosend_ems\Service\MoosendEms.
   *
   * @var \Drupal\moosend_ems\Service\MoosendEms
   *   Moosend ems service.
   */
  protected $moosendEms;

  /**
   * MoosendEmsLists constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal\Core\Config\ConfigFactoryInterface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal\Core\Messenger\MessengerInterface.
   * @param \Drupal\moosend_ems\Service\MoosendEms $moosendEms
   *   Moosend ems service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, MoosendEms $moosendEms) {
    parent::__construct($config_factory);
    $this->messenger = $messenger;
    $this->moosendEms = $moosendEms;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('moosend_ems')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'moosend_ems_lists';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'moosend_ems.enabled_lists',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->moosendEms->getConfig();
    $enabled = $this->config('moosend_ems.enabled_lists')->getRawData();

    $header = [
      'name' => $this->t('List Name'),
      'list_id' => $this->t('List UUID'),
    ];

    $output = $defaultValue = [];

    if (isset($config['api_key'])) {
      $lists = $this->moosendEms->getMailingLists();

      if(is_array($lists)) {
        if ($lists && is_array($lists) &&count($lists) > 0) {
          foreach ($lists as $list) {
            $output[$list->list_id] = [
              'name' => $list->name,
              'list_id' => $list->list_id,
            ];

            $defaultValue[$list->list_id] = isset($enabled[$list->list_id]) && $enabled[$list->list_id] === 1 ? $list->list_id : NULL;
          }
        }
      }
    }

    $form['lists'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $output,
      '#default_value' => $defaultValue,
      '#empty' => $this->t('You must authorize Moosend EMS before enabling a list or there are no lists available on your account.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('moosend_ems.enabled_lists');
    $config->clear('moosend_ems.enabled_lists');
    $enabled = $form_state->getValues()['lists'];
    $lists = $this->moosendEms->getMailingLists();

    foreach ($enabled as $key => $value) {
      $config->set($key, ($value === 0 ? 0 : 1));
    }

    $config->save();

    $this->moosendEms->saveContactLists($lists);

    $this->messenger->addMessage($this->t('Your configuration has been saved'));
  }

}
