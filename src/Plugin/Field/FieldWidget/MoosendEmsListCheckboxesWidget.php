<?php

namespace Drupal\moosend_ems\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;

/**
 * Plugin implementation of the 'moosend_ems_lists_default' widget.
 *
 * @FieldWidget(
 *   id = "moosend_ems_lists_checkbox",
 *   label = @Translation("Check boxes/radio buttons"),
 *   field_types = {
 *     "moosend_ems_lists"
 *   },
 *   multiple_values = TRUE
 * )
 */
class MoosendEmsListCheckboxesWidget extends OptionsButtonsWidget {
}