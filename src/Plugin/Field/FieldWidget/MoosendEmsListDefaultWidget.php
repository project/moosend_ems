<?php

namespace Drupal\moosend_ems\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'moosend_ems_lists_default' widget.
 *
 * @FieldWidget(
 *   id = "moosend_ems_lists_default",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "moosend_ems_lists"
 *   },
 *   multiple_values = TRUE
 * )
 */
class MoosendEmsListDefaultWidget extends OptionsSelectWidget {
}