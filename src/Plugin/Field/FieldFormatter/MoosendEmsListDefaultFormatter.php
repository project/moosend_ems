<?php

namespace Drupal\moosend_ems\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'moosend_ems_lists_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "moosend_ems_lists_formatter",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "moosend_ems_lists",
 *   }
 * )
 */
class MoosendEmsListDefaultFormatter extends OptionsDefaultFormatter {
}