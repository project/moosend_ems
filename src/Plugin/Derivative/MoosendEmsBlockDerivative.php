<?php

namespace Drupal\moosend_ems\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\moosend_ems\Service\MoosendEms;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for Moosend EMS blocks.
 *
 * @see \Drupal\moosend_ems\Plugin\Block\MoosendEmsBlock
 */
class MoosendEmsBlockDerivative extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\moosend_ems\Service\MoosendEms.
   *
   * @var \Drupal\moosend_ems\Service\MoosendEms
   *   Moosend ems service.
   */
  protected $moosendEms;

  /**
   * MoosendEmsBlockDerivative constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactoryInterface.
   * @param \Drupal\moosend_ems\Service\MoosendEms $moosendEms
   *   Moosend ems service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, MoosendEms $moosendEms) {
    $this->configFactory = $configFactory;
    $this->moosendEms = $moosendEms;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory'),
      $container->get('moosend_ems')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $lists = $this->moosendEms->getEnabledMailingLists(false);

    if ($lists) {
      foreach ($lists as $id => $value) {
        if ($value->enabled === true) {
          $this->derivatives[$id] = $base_plugin_definition;
          $this->derivatives[$id]['admin_label'] = $lists[$id]->name . '  Signup Block';
        }
      }
    }

    return $this->derivatives;
  }

}
