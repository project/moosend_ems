<?php

namespace Drupal\moosend_ems\Plugin;

use \ArrayAccess;

class Paging implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $moosendPluginName = 'Paging';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $moosendPropTypes = [
        'current_page' => 'double',
        'page_size' => 'double',
        'sort_expression' => 'string',
        'sort_is_ascending' => 'bool',
        'total_page_count' => 'double',
        'total_results' => 'double'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $moosendPropFormats = [
        'current_page' => 'double',
        'page_size' => 'double',
        'sort_expression' => null,
        'sort_is_ascending' => null,
        'total_page_count' => 'double',
        'total_results' => 'double'
    ];

    public static function moosendPropTypes()
    {
        return self::$moosendPropTypes;
    }

    public static function moosendPropFormats()
    {
        return self::$moosendPropFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'current_page' => 'CurrentPage',
        'page_size' => 'PageSize',
        'sort_expression' => 'SortExpression',
        'sort_is_ascending' => 'SortIsAscending',
        'total_page_count' => 'TotalPageCount',
        'total_results' => 'TotalResults'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'current_page' => 'setCurrentPage',
        'page_size' => 'setPageSize',
        'sort_expression' => 'setSortExpression',
        'sort_is_ascending' => 'setSortIsAscending',
        'total_page_count' => 'setTotalPageCount',
        'total_results' => 'setTotalResults'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'current_page' => 'getCurrentPage',
        'page_size' => 'getPageSize',
        'sort_expression' => 'getSortExpression',
        'sort_is_ascending' => 'getSortIsAscending',
        'total_page_count' => 'getTotalPageCount',
        'total_results' => 'getTotalResults'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['current_page'] = isset($data['current_page']) ? $data['current_page'] : null;
        $this->container['page_size'] = isset($data['page_size']) ? $data['page_size'] : null;
        $this->container['sort_expression'] = isset($data['sort_expression']) ? $data['sort_expression'] : null;
        $this->container['sort_is_ascending'] = isset($data['sort_is_ascending']) ? $data['sort_is_ascending'] : null;
        $this->container['total_page_count'] = isset($data['total_page_count']) ? $data['total_page_count'] : null;
        $this->container['total_results'] = isset($data['total_results']) ? $data['total_results'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets current_page
     * @return double
     */
    public function getCurrentPage()
    {
        return $this->container['current_page'];
    }

    /**
     * Sets current_page
     * @param double $current_page 
     * @return $this
     */
    public function setCurrentPage($current_page)
    {
        $this->container['current_page'] = $current_page;

        return $this;
    }

    /**
     * Gets page_size
     * @return double
     */
    public function getPageSize()
    {
        return $this->container['page_size'];
    }

    /**
     * Sets page_size
     * @param double $page_size 
     * @return $this
     */
    public function setPageSize($page_size)
    {
        $this->container['page_size'] = $page_size;

        return $this;
    }

    /**
     * Gets sort_expression
     * @return string
     */
    public function getSortExpression()
    {
        return $this->container['sort_expression'];
    }

    /**
     * Sets sort_expression
     * @param string $sort_expression 
     * @return $this
     */
    public function setSortExpression($sort_expression)
    {
        $this->container['sort_expression'] = $sort_expression;

        return $this;
    }

    /**
     * Gets sort_is_ascending
     * @return bool
     */
    public function getSortIsAscending()
    {
        return $this->container['sort_is_ascending'];
    }

    /**
     * Sets sort_is_ascending
     * @param bool $sort_is_ascending 
     * @return $this
     */
    public function setSortIsAscending($sort_is_ascending)
    {
        $this->container['sort_is_ascending'] = $sort_is_ascending;

        return $this;
    }

    /**
     * Gets total_page_count
     * @return double
     */
    public function getTotalPageCount()
    {
        return $this->container['total_page_count'];
    }

    /**
     * Sets total_page_count
     * @param double $total_page_count 
     * @return $this
     */
    public function setTotalPageCount($total_page_count)
    {
        $this->container['total_page_count'] = $total_page_count;

        return $this;
    }

    /**
     * Gets total_results
     * @return double
     */
    public function getTotalResults()
    {
        return $this->container['total_results'];
    }

    /**
     * Sets total_results
     * @param double $total_results 
     * @return $this
     */
    public function setTotalResults($total_results)
    {
        $this->container['total_results'] = $total_results;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet(mixed $offset): mixed
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(Drupal\moosend_ems\Plugin\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(Drupal\moosend_ems\Plugin\ObjectSerializer::sanitizeForSerialization($this));
    }
}


