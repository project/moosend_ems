<?php

namespace Drupal\moosend_ems\Plugin;

use Drupal\moosend_ems\Plugin\ApiClient;
use Drupal\moosend_ems\Plugin\ApiException;

class SegmentsApi
{
    /**
     * API Client
     *
     * @var Drupal\moosend_ems\Plugin\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param Drupal\moosend_ems\Plugin\ApiClient|null $apiClient The api client to use
     */
    public function __construct(Drupal\moosend_ems\Plugin\ApiClient $apiClient = null)
    {
        if ($apiClient === null) {
            $apiClient = new ApiClient();
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return Drupal\moosend_ems\Plugin\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param Drupal\moosend_ems\Plugin\ApiClient $apiClient set the API client
     *
     * @return SegmentsApi
     */
    public function setApiClient(Drupal\moosend_ems\Plugin\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation addingCriteriaToSegments
     *
     * Adding criteria to segments
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param string $segment_id The ID of the segment to update. (required)
     * @param Drupal\moosend_ems\Plugin\Model\AddingCriteriaToSegmentsRequest $body  (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return Drupal\moosend_ems\Plugin\Model\AddingCriteriaToSegmentsResponse
     */
    public function addingCriteriaToSegments($format, $mailing_list_id, $apikey, $segment_id, $body)
    {
        list($response) = $this->addingCriteriaToSegmentsWithHttpInfo($format, $mailing_list_id, $apikey, $segment_id, $body);
        return $response;
    }

    /**
     * Operation addingCriteriaToSegmentsWithHttpInfo
     *
     * Adding criteria to segments
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param string $segment_id The ID of the segment to update. (required)
     * @param Drupal\moosend_ems\Plugin\Model\AddingCriteriaToSegmentsRequest $body  (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return array of Drupal\moosend_ems\Plugin\Model\AddingCriteriaToSegmentsResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function addingCriteriaToSegmentsWithHttpInfo($format, $mailing_list_id, $apikey, $segment_id, $body)
    {
        // verify the required parameter 'format' is set
        if ($format === null) {
            throw new \InvalidArgumentException('Missing the required parameter $format when calling addingCriteriaToSegments');
        }
        // verify the required parameter 'mailing_list_id' is set
        if ($mailing_list_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $mailing_list_id when calling addingCriteriaToSegments');
        }
        // verify the required parameter 'apikey' is set
        if ($apikey === null) {
            throw new \InvalidArgumentException('Missing the required parameter $apikey when calling addingCriteriaToSegments');
        }
        // verify the required parameter 'segment_id' is set
        if ($segment_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $segment_id when calling addingCriteriaToSegments');
        }
        // verify the required parameter 'body' is set
        if ($body === null) {
            throw new \InvalidArgumentException('Missing the required parameter $body when calling addingCriteriaToSegments');
        }
        // parse inputs
        $resourcePath = "/lists/{MailingListID}/segments/{SegmentID}/criteria/add.{Format}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // query params
        if ($apikey !== null) {
            $queryParams['apikey'] = $this->apiClient->getSerializer()->toQueryValue($apikey);
        }
        // path params
        if ($format !== null) {
            $resourcePath = str_replace(
                "{" . "Format" . "}",
                $this->apiClient->getSerializer()->toPathValue($format),
                $resourcePath
            );
        }
        // path params
        if ($mailing_list_id !== null) {
            $resourcePath = str_replace(
                "{" . "MailingListID" . "}",
                $this->apiClient->getSerializer()->toPathValue($mailing_list_id),
                $resourcePath
            );
        }
        // path params
        if ($segment_id !== null) {
            $resourcePath = str_replace(
                "{" . "SegmentID" . "}",
                $this->apiClient->getSerializer()->toPathValue($segment_id),
                $resourcePath
            );
        }
        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                'Drupal\moosend_ems\Plugin\Model\AddingCriteriaToSegmentsResponse',
                '/lists/{MailingListID}/segments/{SegmentID}/criteria/add.{Format}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, 'Drupal\moosend_ems\Plugin\Model\AddingCriteriaToSegmentsResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), 'Drupal\moosend_ems\Plugin\Model\AddingCriteriaToSegmentsResponse', $e->getResponseHeaders());
                $e->setResponseObject($data);
                break;
            }

            throw $e;
        }
    }

    /**
     * Operation creatingANewSegment
     *
     * Creating a new segment
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param Drupal\moosend_ems\Plugin\Model\CreatingANewSegmentRequest $body  (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return Drupal\moosend_ems\Plugin\Model\CreatingANewSegmentResponse
     */
    public function creatingANewSegment($format, $mailing_list_id, $apikey, $body)
    {
        list($response) = $this->creatingANewSegmentWithHttpInfo($format, $mailing_list_id, $apikey, $body);
        return $response;
    }

    /**
     * Operation creatingANewSegmentWithHttpInfo
     *
     * Creating a new segment
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param Drupal\moosend_ems\Plugin\Model\CreatingANewSegmentRequest $body  (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return array of Drupal\moosend_ems\Plugin\Model\CreatingANewSegmentResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function creatingANewSegmentWithHttpInfo($format, $mailing_list_id, $apikey, $body)
    {
        // verify the required parameter 'format' is set
        if ($format === null) {
            throw new \InvalidArgumentException('Missing the required parameter $format when calling creatingANewSegment');
        }
        // verify the required parameter 'mailing_list_id' is set
        if ($mailing_list_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $mailing_list_id when calling creatingANewSegment');
        }
        // verify the required parameter 'apikey' is set
        if ($apikey === null) {
            throw new \InvalidArgumentException('Missing the required parameter $apikey when calling creatingANewSegment');
        }
        // verify the required parameter 'body' is set
        if ($body === null) {
            throw new \InvalidArgumentException('Missing the required parameter $body when calling creatingANewSegment');
        }
        // parse inputs
        $resourcePath = "/lists/{MailingListID}/segments/create.{Format}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // query params
        if ($apikey !== null) {
            $queryParams['apikey'] = $this->apiClient->getSerializer()->toQueryValue($apikey);
        }
        // path params
        if ($format !== null) {
            $resourcePath = str_replace(
                "{" . "Format" . "}",
                $this->apiClient->getSerializer()->toPathValue($format),
                $resourcePath
            );
        }
        // path params
        if ($mailing_list_id !== null) {
            $resourcePath = str_replace(
                "{" . "MailingListID" . "}",
                $this->apiClient->getSerializer()->toPathValue($mailing_list_id),
                $resourcePath
            );
        }
        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                'Drupal\moosend_ems\Plugin\Model\CreatingANewSegmentResponse',
                '/lists/{MailingListID}/segments/create.{Format}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, 'Drupal\moosend_ems\Plugin\Model\CreatingANewSegmentResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), 'Drupal\moosend_ems\Plugin\Model\CreatingANewSegmentResponse', $e->getResponseHeaders());
                $e->setResponseObject($data);
                break;
            }

            throw $e;
        }
    }

    /**
     * Operation deletingASegment
     *
     * Deleting A Segment
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param string $segment_id The ID of the segment to update. (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return Drupal\moosend_ems\Plugin\Model\DeletingASegmentResponse
     */
    public function deletingASegment($format, $mailing_list_id, $apikey, $segment_id)
    {
        list($response) = $this->deletingASegmentWithHttpInfo($format, $mailing_list_id, $apikey, $segment_id);
        return $response;
    }

    /**
     * Operation deletingASegmentWithHttpInfo
     *
     * Deleting A Segment
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param string $segment_id The ID of the segment to update. (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return array of Drupal\moosend_ems\Plugin\Model\DeletingASegmentResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function deletingASegmentWithHttpInfo($format, $mailing_list_id, $apikey, $segment_id)
    {
        // verify the required parameter 'format' is set
        if ($format === null) {
            throw new \InvalidArgumentException('Missing the required parameter $format when calling deletingASegment');
        }
        // verify the required parameter 'mailing_list_id' is set
        if ($mailing_list_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $mailing_list_id when calling deletingASegment');
        }
        // verify the required parameter 'apikey' is set
        if ($apikey === null) {
            throw new \InvalidArgumentException('Missing the required parameter $apikey when calling deletingASegment');
        }
        // verify the required parameter 'segment_id' is set
        if ($segment_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $segment_id when calling deletingASegment');
        }
        // parse inputs
        $resourcePath = "/lists/{MailingListID}/segments/{SegmentID}/delete.{Format}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // query params
        if ($apikey !== null) {
            $queryParams['apikey'] = $this->apiClient->getSerializer()->toQueryValue($apikey);
        }
        // path params
        if ($format !== null) {
            $resourcePath = str_replace(
                "{" . "Format" . "}",
                $this->apiClient->getSerializer()->toPathValue($format),
                $resourcePath
            );
        }
        // path params
        if ($mailing_list_id !== null) {
            $resourcePath = str_replace(
                "{" . "MailingListID" . "}",
                $this->apiClient->getSerializer()->toPathValue($mailing_list_id),
                $resourcePath
            );
        }
        // path params
        if ($segment_id !== null) {
            $resourcePath = str_replace(
                "{" . "SegmentID" . "}",
                $this->apiClient->getSerializer()->toPathValue($segment_id),
                $resourcePath
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'DELETE',
                $queryParams,
                $httpBody,
                $headerParams,
                'Drupal\moosend_ems\Plugin\Model\DeletingASegmentResponse',
                '/lists/{MailingListID}/segments/{SegmentID}/delete.{Format}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, 'Drupal\moosend_ems\Plugin\Model\DeletingASegmentResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), 'Drupal\moosend_ems\Plugin\Model\DeletingASegmentResponse', $e->getResponseHeaders());
                $e->setResponseObject($data);
                break;
            }

            throw $e;
        }
    }

    /**
     * Operation gettingSegmentDetails
     *
     * Getting segment details
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs (required)
     * @param string $segment_id The ID of the segment to fetch results for. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return Drupal\moosend_ems\Plugin\Model\GettingSegmentDetailsResponse
     */
    public function gettingSegmentDetails($format, $mailing_list_id, $segment_id, $apikey)
    {
        list($response) = $this->gettingSegmentDetailsWithHttpInfo($format, $mailing_list_id, $segment_id, $apikey);
        return $response;
    }

    /**
     * Operation gettingSegmentDetailsWithHttpInfo
     *
     * Getting segment details
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs (required)
     * @param string $segment_id The ID of the segment to fetch results for. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return array of Drupal\moosend_ems\Plugin\Model\GettingSegmentDetailsResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function gettingSegmentDetailsWithHttpInfo($format, $mailing_list_id, $segment_id, $apikey)
    {
        // verify the required parameter 'format' is set
        if ($format === null) {
            throw new \InvalidArgumentException('Missing the required parameter $format when calling gettingSegmentDetails');
        }
        // verify the required parameter 'mailing_list_id' is set
        if ($mailing_list_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $mailing_list_id when calling gettingSegmentDetails');
        }
        // verify the required parameter 'segment_id' is set
        if ($segment_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $segment_id when calling gettingSegmentDetails');
        }
        // verify the required parameter 'apikey' is set
        if ($apikey === null) {
            throw new \InvalidArgumentException('Missing the required parameter $apikey when calling gettingSegmentDetails');
        }
        // parse inputs
        $resourcePath = "/lists/{MailingListID}/segments/{SegmentID}/details.{Format}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // query params
        if ($apikey !== null) {
            $queryParams['apikey'] = $this->apiClient->getSerializer()->toQueryValue($apikey);
        }
        // path params
        if ($format !== null) {
            $resourcePath = str_replace(
                "{" . "Format" . "}",
                $this->apiClient->getSerializer()->toPathValue($format),
                $resourcePath
            );
        }
        // path params
        if ($mailing_list_id !== null) {
            $resourcePath = str_replace(
                "{" . "MailingListID" . "}",
                $this->apiClient->getSerializer()->toPathValue($mailing_list_id),
                $resourcePath
            );
        }
        // path params
        if ($segment_id !== null) {
            $resourcePath = str_replace(
                "{" . "SegmentID" . "}",
                $this->apiClient->getSerializer()->toPathValue($segment_id),
                $resourcePath
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                'Drupal\moosend_ems\Plugin\Model\GettingSegmentDetailsResponse',
                '/lists/{MailingListID}/segments/{SegmentID}/details.{Format}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, 'Drupal\moosend_ems\Plugin\Model\GettingSegmentDetailsResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), 'Drupal\moosend_ems\Plugin\Model\GettingSegmentDetailsResponse', $e->getResponseHeaders());
                $e->setResponseObject($data);
                break;
            }

            throw $e;
        }
    }

    /**
     * Operation gettingSegmentSubscribers
     *
     * Getting segment subscribers
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs (required)
     * @param string $segment_id The ID of the segment to fetch results for. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return Drupal\moosend_ems\Plugin\Model\GettingSegmentSubscribersResponse
     */
    public function gettingSegmentSubscribers($format, $mailing_list_id, $segment_id, $apikey)
    {
        list($response) = $this->gettingSegmentSubscribersWithHttpInfo($format, $mailing_list_id, $segment_id, $apikey);
        return $response;
    }

    /**
     * Operation gettingSegmentSubscribersWithHttpInfo
     *
     * Getting segment subscribers
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs (required)
     * @param string $segment_id The ID of the segment to fetch results for. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return array of Drupal\moosend_ems\Plugin\Model\GettingSegmentSubscribersResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function gettingSegmentSubscribersWithHttpInfo($format, $mailing_list_id, $segment_id, $apikey)
    {
        // verify the required parameter 'format' is set
        if ($format === null) {
            throw new \InvalidArgumentException('Missing the required parameter $format when calling gettingSegmentSubscribers');
        }
        // verify the required parameter 'mailing_list_id' is set
        if ($mailing_list_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $mailing_list_id when calling gettingSegmentSubscribers');
        }
        // verify the required parameter 'segment_id' is set
        if ($segment_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $segment_id when calling gettingSegmentSubscribers');
        }
        // verify the required parameter 'apikey' is set
        if ($apikey === null) {
            throw new \InvalidArgumentException('Missing the required parameter $apikey when calling gettingSegmentSubscribers');
        }
        // parse inputs
        $resourcePath = "/lists/{MailingListID}/segments/{SegmentID}/members.{Format}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // query params
        if ($apikey !== null) {
            $queryParams['apikey'] = $this->apiClient->getSerializer()->toQueryValue($apikey);
        }
        // path params
        if ($format !== null) {
            $resourcePath = str_replace(
                "{" . "Format" . "}",
                $this->apiClient->getSerializer()->toPathValue($format),
                $resourcePath
            );
        }
        // path params
        if ($mailing_list_id !== null) {
            $resourcePath = str_replace(
                "{" . "MailingListID" . "}",
                $this->apiClient->getSerializer()->toPathValue($mailing_list_id),
                $resourcePath
            );
        }
        // path params
        if ($segment_id !== null) {
            $resourcePath = str_replace(
                "{" . "SegmentID" . "}",
                $this->apiClient->getSerializer()->toPathValue($segment_id),
                $resourcePath
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                'Drupal\moosend_ems\Plugin\Model\GettingSegmentSubscribersResponse',
                '/lists/{MailingListID}/segments/{SegmentID}/members.{Format}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, 'Drupal\moosend_ems\Plugin\Model\GettingSegmentSubscribersResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), 'Drupal\moosend_ems\Plugin\Model\GettingSegmentSubscribersResponse', $e->getResponseHeaders());
                $e->setResponseObject($data);
                break;
            }

            throw $e;
        }
    }

    /**
     * Operation gettingSegments
     *
     * Getting segments
     *
     * @param string $format  (required)
     * @param string $mailing_list_id  (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return Drupal\moosend_ems\Plugin\Model\GettingSegmentsResponse
     */
    public function gettingSegments($format, $mailing_list_id, $apikey)
    {
        list($response) = $this->gettingSegmentsWithHttpInfo($format, $mailing_list_id, $apikey);
        return $response;
    }

    /**
     * Operation gettingSegmentsWithHttpInfo
     *
     * Getting segments
     *
     * @param string $format  (required)
     * @param string $mailing_list_id  (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return array of Drupal\moosend_ems\Plugin\Model\GettingSegmentsResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function gettingSegmentsWithHttpInfo($format, $mailing_list_id, $apikey)
    {
        // verify the required parameter 'format' is set
        if ($format === null) {
            throw new \InvalidArgumentException('Missing the required parameter $format when calling gettingSegments');
        }
        // verify the required parameter 'mailing_list_id' is set
        if ($mailing_list_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $mailing_list_id when calling gettingSegments');
        }
        // verify the required parameter 'apikey' is set
        if ($apikey === null) {
            throw new \InvalidArgumentException('Missing the required parameter $apikey when calling gettingSegments');
        }
        // parse inputs
        $resourcePath = "/lists/{MailingListID}/segments.{Format}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // query params
        if ($apikey !== null) {
            $queryParams['apikey'] = $this->apiClient->getSerializer()->toQueryValue($apikey);
        }
        // path params
        if ($format !== null) {
            $resourcePath = str_replace(
                "{" . "Format" . "}",
                $this->apiClient->getSerializer()->toPathValue($format),
                $resourcePath
            );
        }
        // path params
        if ($mailing_list_id !== null) {
            $resourcePath = str_replace(
                "{" . "MailingListID" . "}",
                $this->apiClient->getSerializer()->toPathValue($mailing_list_id),
                $resourcePath
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                'Drupal\moosend_ems\Plugin\Model\GettingSegmentsResponse',
                '/lists/{MailingListID}/segments.{Format}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, 'Drupal\moosend_ems\Plugin\Model\GettingSegmentsResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), 'Drupal\moosend_ems\Plugin\Model\GettingSegmentsResponse', $e->getResponseHeaders());
                $e->setResponseObject($data);
                break;
            }

            throw $e;
        }
    }

    /**
     * Operation updatingASegment
     *
     * Updating a segment
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param string $segment_id The ID of the segment to update. (required)
     * @param Drupal\moosend_ems\Plugin\Model\UpdatingASegmentRequest $body  (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return Drupal\moosend_ems\Plugin\Model\UpdatingASegmentResponse
     */
    public function updatingASegment($format, $mailing_list_id, $apikey, $segment_id, $body)
    {
        list($response) = $this->updatingASegmentWithHttpInfo($format, $mailing_list_id, $apikey, $segment_id, $body);
        return $response;
    }

    /**
     * Operation updatingASegmentWithHttpInfo
     *
     * Updating a segment
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param string $segment_id The ID of the segment to update. (required)
     * @param Drupal\moosend_ems\Plugin\Model\UpdatingASegmentRequest $body  (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return array of Drupal\moosend_ems\Plugin\Model\UpdatingASegmentResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function updatingASegmentWithHttpInfo($format, $mailing_list_id, $apikey, $segment_id, $body)
    {
        // verify the required parameter 'format' is set
        if ($format === null) {
            throw new \InvalidArgumentException('Missing the required parameter $format when calling updatingASegment');
        }
        // verify the required parameter 'mailing_list_id' is set
        if ($mailing_list_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $mailing_list_id when calling updatingASegment');
        }
        // verify the required parameter 'apikey' is set
        if ($apikey === null) {
            throw new \InvalidArgumentException('Missing the required parameter $apikey when calling updatingASegment');
        }
        // verify the required parameter 'segment_id' is set
        if ($segment_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $segment_id when calling updatingASegment');
        }
        // verify the required parameter 'body' is set
        if ($body === null) {
            throw new \InvalidArgumentException('Missing the required parameter $body when calling updatingASegment');
        }
        // parse inputs
        $resourcePath = "/lists/{MailingListID}/segments/{SegmentID}/update.{Format}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // query params
        if ($apikey !== null) {
            $queryParams['apikey'] = $this->apiClient->getSerializer()->toQueryValue($apikey);
        }
        // path params
        if ($format !== null) {
            $resourcePath = str_replace(
                "{" . "Format" . "}",
                $this->apiClient->getSerializer()->toPathValue($format),
                $resourcePath
            );
        }
        // path params
        if ($mailing_list_id !== null) {
            $resourcePath = str_replace(
                "{" . "MailingListID" . "}",
                $this->apiClient->getSerializer()->toPathValue($mailing_list_id),
                $resourcePath
            );
        }
        // path params
        if ($segment_id !== null) {
            $resourcePath = str_replace(
                "{" . "SegmentID" . "}",
                $this->apiClient->getSerializer()->toPathValue($segment_id),
                $resourcePath
            );
        }
        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                'Drupal\moosend_ems\Plugin\Model\UpdatingASegmentResponse',
                '/lists/{MailingListID}/segments/{SegmentID}/update.{Format}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, 'Drupal\moosend_ems\Plugin\Model\UpdatingASegmentResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), 'Drupal\moosend_ems\Plugin\Model\UpdatingASegmentResponse', $e->getResponseHeaders());
                $e->setResponseObject($data);
                break;
            }

            throw $e;
        }
    }

    /**
     * Operation updatingSegmentCriteria
     *
     * Updating segment criteria
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param string $segment_id The ID of the segment to update. (required)
     * @param double $criteria_id The ID of the criterion to process. (required)
     * @param Drupal\moosend_ems\Plugin\Model\UpdatingSegmentCriteriaRequest $body  (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return Drupal\moosend_ems\Plugin\Model\UpdatingSegmentCriteriaResponse
     */
    public function updatingSegmentCriteria($format, $mailing_list_id, $apikey, $segment_id, $criteria_id, $body)
    {
        list($response) = $this->updatingSegmentCriteriaWithHttpInfo($format, $mailing_list_id, $apikey, $segment_id, $criteria_id, $body);
        return $response;
    }

    /**
     * Operation updatingSegmentCriteriaWithHttpInfo
     *
     * Updating segment criteria
     *
     * @param string $format  (required)
     * @param string $mailing_list_id The ID of the mailing list the specified segment belongs. (required)
     * @param string $apikey You may find your API Key or generate a new one in your account settings. (required)
     * @param string $segment_id The ID of the segment to update. (required)
     * @param double $criteria_id The ID of the criterion to process. (required)
     * @param Drupal\moosend_ems\Plugin\Model\UpdatingSegmentCriteriaRequest $body  (required)
     * @throws Drupal\moosend_ems\Plugin\ApiException on non-2xx response
     * @return array of Drupal\moosend_ems\Plugin\Model\UpdatingSegmentCriteriaResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function updatingSegmentCriteriaWithHttpInfo($format, $mailing_list_id, $apikey, $segment_id, $criteria_id, $body)
    {
        // verify the required parameter 'format' is set
        if ($format === null) {
            throw new \InvalidArgumentException('Missing the required parameter $format when calling updatingSegmentCriteria');
        }
        // verify the required parameter 'mailing_list_id' is set
        if ($mailing_list_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $mailing_list_id when calling updatingSegmentCriteria');
        }
        // verify the required parameter 'apikey' is set
        if ($apikey === null) {
            throw new \InvalidArgumentException('Missing the required parameter $apikey when calling updatingSegmentCriteria');
        }
        // verify the required parameter 'segment_id' is set
        if ($segment_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $segment_id when calling updatingSegmentCriteria');
        }
        // verify the required parameter 'criteria_id' is set
        if ($criteria_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $criteria_id when calling updatingSegmentCriteria');
        }
        // verify the required parameter 'body' is set
        if ($body === null) {
            throw new \InvalidArgumentException('Missing the required parameter $body when calling updatingSegmentCriteria');
        }
        // parse inputs
        $resourcePath = "/lists/{MailingListID}/segments/{SegmentID}/criteria/{CriteriaID}/update.{Format}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // query params
        if ($apikey !== null) {
            $queryParams['apikey'] = $this->apiClient->getSerializer()->toQueryValue($apikey);
        }
        // path params
        if ($format !== null) {
            $resourcePath = str_replace(
                "{" . "Format" . "}",
                $this->apiClient->getSerializer()->toPathValue($format),
                $resourcePath
            );
        }
        // path params
        if ($mailing_list_id !== null) {
            $resourcePath = str_replace(
                "{" . "MailingListID" . "}",
                $this->apiClient->getSerializer()->toPathValue($mailing_list_id),
                $resourcePath
            );
        }
        // path params
        if ($segment_id !== null) {
            $resourcePath = str_replace(
                "{" . "SegmentID" . "}",
                $this->apiClient->getSerializer()->toPathValue($segment_id),
                $resourcePath
            );
        }
        // path params
        if ($criteria_id !== null) {
            $resourcePath = str_replace(
                "{" . "CriteriaID" . "}",
                $this->apiClient->getSerializer()->toPathValue($criteria_id),
                $resourcePath
            );
        }
        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                'Drupal\moosend_ems\Plugin\Model\UpdatingSegmentCriteriaResponse',
                '/lists/{MailingListID}/segments/{SegmentID}/criteria/{CriteriaID}/update.{Format}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, 'Drupal\moosend_ems\Plugin\Model\UpdatingSegmentCriteriaResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), 'Drupal\moosend_ems\Plugin\Model\UpdatingSegmentCriteriaResponse', $e->getResponseHeaders());
                $e->setResponseObject($data);
                break;
            }

            throw $e;
        }
    }
}
