<?php

namespace Drupal\moosend_ems\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Drupal\moosend_ems\Plugin\SubscribersApi;
use Drupal\moosend_ems\Plugin\MailingListsApi;
use Drupal\moosend_ems\Plugin\AddingSubscribersRequest;
use Drupal\moosend_ems\Plugin\UnsubscribingSubscribersFromMailingListRequest;

/**
 * Class MoosendEms.
 *
 * Class to handle API calls to Moosend EMS.
 */
class MoosendEms {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Cache\CacheBackendInterface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   *   Drupal cache.
   */
  protected $cache;

  /**
   * Drupal\Core\Config\ConfigFactory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   *   Drupal config.
   */
  protected $config;

  /**
   * Drupal\Core\Session\AccountProxy.
   *
   * @var \Drupal\Core\Session\AccountProxy
   *   Drupal current user.
   */
  protected $currentUser;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Drupal logging.
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Drupal\Core\Site\Settings.
   *
   * @var \Drupal\Core\Site\Settings
   *   Drupal site settings.
   */
  protected $settings;

  /**
   * GuzzleHttp\Client.
   *
   * @var \GuzzleHttp\Client
   *   Guzzle HTTP Client.
   */
  protected $httpClient;

  /**
   * \Drupal\Core\Extension\ModuleHandlerInterface
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   *   Module handler interface
   */
  protected $moduleHandler;

  /**
   * The Moosend EMS v3 API endpoint.
   *
   * @var string
   */
  protected $apiUrl = 'https://api.cc.email/v3';

  /**
   * The URL to use for authorization.
   *
   * @var string
   */
  protected $authUrl = 'https://authz.moosendems.com/oauth2/default/v1/authorize';

  /**
   * The URL to use for token oauth.
   *
   * @var string
   */
  protected $tokenUrl = 'https://authz.moosendems.com/oauth2/default/v1/token';

  /**
   * Constructs the class.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The interface for cache implementations.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The configuration object factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The factory for logging channels.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The runtime messages sent out to individual users on the page.
   * @param \Drupal\Core\Site\Settings $settings
   *   The read settings that are initialized with the class.
   * @param \GuzzleHttp\Client $httpClient
   *   The client for sending HTTP requests.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(CacheBackendInterface $cache, ConfigFactory $config, AccountProxy $currentUser, LoggerChannelFactoryInterface $loggerFactory, MessengerInterface $messenger, Settings $settings, Client $httpClient, ModuleHandlerInterface $moduleHandler) {
    $this->cache = $cache;
    $this->config = $config;
    $this->currentUser = $currentUser;
    $this->loggerFactory = $loggerFactory->get('moosend_ems');
    $this->messenger = $messenger;
    $this->settings = $settings;
    $this->httpClient = $httpClient;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Returns the configurations for the class.
   *
   * @return array
   *   Returns an array with all configuration settings.
   */
  public function getConfig() {
    // Get our settings from settings.php.
    $settings = $this->settings::get('moosend_ems');
    $api_key = isset($settings['api_key']) ? $settings['api_key'] : NULL;
    $configType = 'settings.php';

    // If nothing is in settings.php, let's check our config files.
    if (!$settings) {
      $api_key = $this->config->get('moosend_ems.config')->get('api_key');
      $configType = 'config';
    }

    if (!$this->moduleHandler->moduleExists('automated_cron') && !$this->moduleHandler->moduleExists('ultimate_cron') && (int)$this->currentUser->id() !== 0 && $this->currentUser->hasPermission('administer moosend ems configuration')) {
      $this->messenger->addMessage($this->t('It is recommended to install automated_cron or make sure that cron is run regularly to refresh access tokens from Moosend EMS API.'), 'warning');
    }


    return [
      'api_key' => $api_key,
      'config_type' => $configType, // Application api_key and other info found in settings.php or via config
      'refresh_token' => $this->config->get('moosend_ems.tokens')->get('refresh_token'),
      'authentication_url' => $this->authUrl,
      'token_url' => $this->tokenUrl,
      'contact_url' => $this->apiUrl . '/contacts',
      'contact_lists_url' => $this->apiUrl . '/contact_lists',
      'campaigns_url' => $this->apiUrl . '/emails',
      'campaign_activity_url' => $this->apiUrl . '/emails/activities',

      // Add fields to configuration for signup form block configuration
      // @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
      'fields' => [
        'name' => 'Name',
        'mobile' => 'Mobile',
      ],
    ];
  }

  public function base64UrlEncode($string) {
    $base64 = base64_encode($string);
    $base64 = trim($base64, '=');
    $base64url = strtr($base64, '+/', '-_');

    return $base64url;
  }

  /**
   * Generates a random PKCE Code Verifier.
   *
   * https://datatracker.ietf.org/doc/html/rfc7636#section-4.1
   * https://docs.moosend.com/developers/api-documentation/en/index-en.html
   *
   * @return string
   */
  public function generateCodeVerifier() {
    $random = bin2hex(openssl_random_pseudo_bytes(32));
    $verifier = $this->base64UrlEncode(pack('H*', $random));

    return $verifier;
  }

  /**
   * Return the encoded code challenge for the PKCE Code Verifier to send to API Authorization Endpoint.
   *
   * https://docs.moosend.com/developers/api-documentation/en/index-en.html
   *
   * @param string $codeVerifier
   * @return string
   */
  public function getCodeChallenge($codeVerifier) {
    return $this->base64UrlEncode(pack('H*', hash('sha256', $codeVerifier)));
  }

  /**
   * Shared method to generate the rest of the request body.
   * 
   * @NOTE that email_address, permission_to_send are not added hear since the fields are
   * different per api call type. For example, the list_memberships, the email_address field. 
   * 
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   *
   * @param array $data - posted data from our form
   * @param object $body - An object already generated.
   * @return object $body
   */
  protected function buildResponseBody(array $data, object $body) {

    $fields = $this->getConfig()['fields'];

    foreach ($fields as $field => $name) {
      if (isset($data[$field]) && $data[$field]) {
        $body->{$field} = $data[$field];
      }
    }


    if (isset($data['custom_fields']) && count($data['custom_fields']) > 0) {
      foreach ($data['custom_fields'] as $id => $value) {
        $body->custom_fields[] = ['custom_field_id' => $id, 'value' => $value];
      }
    }

    return $body;
  }

  /**
   * Creates a new contact by posting to Moosend EMS API.
   *
   * @param array $data
   *   Array of data to send to Moosend EMS.
   *    Requires 'email_address' key.
   *    Can also accept 'name' and 'mobile'.
   * @param array $listIDs
   *   An array of list UUIDs where we want to add this contact.
   *
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html#!/Contacts/createContact
   */
  private function createContact(array $data, $listIDs) {
    $config = $this->getConfig();

    $body = (object) [
      'email_address' => (object) [
        'address' => NULL,
        'permission_to_send' => NULL,
      ],
      'name' => NULL,
      'mobile' => NULL,
      'create_source' => NULL,
      'list_memberships' => NULL,
    ];

    $body = $this->buildResponseBody($data, $body);

    // Add our required fields.
    $body->email_address->address = $data['email_address'];
    $body->email_address->permission_to_send = 'implicit';
    $body->list_memberships = $listIDs;
    $body->create_source = 'Account';

    $this->moduleHandler->invokeAll('moosend_ems_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('moosend_ems_contact_create_data_alter', [$data, &$body]);

    try {
      $response = $this->httpClient->request('POST', $config['contact_url'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
        'body' => json_encode($body),
      ]);

      $this->handleResponse($response, 'createContact');
    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e);

      // Return the error to show an error on form submission
      return ['error' => $e];
    }
  }


  /**
   * Fetch the details of a single campaign.
   *
   * @param string $id
   *   The id of the campaign.
   *
   * @return mixed
   *   An stdClass of the campaign.
   * @throws \GuzzleHttp\Exception\GuzzleException
   * 
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   */
  public function getCampaign(string $id) {
    $config = $this->getConfig();
    try {
      $response = $this->httpClient->request('GET', $config['campaigns_url'] . '/' . $id, [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());
      return $json;
    }
    catch (RequestException $e) {
      $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e);
    }
  }

  /**
   * Get the campaign activity details by id.
   *
   * @param string $id
   *   The id of the activity.
   *
   * @return mixed
   *   A stdClass of the activity.
   * @throws \GuzzleHttp\Exception\GuzzleException
   * 
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   */
  public function getCampaignActivity(string $id) {
    $config = $this->getConfig();
    $url = $config['campaign_activity_url'] . '/' . $id;
    $url .= '?include=permalink_url';
    try {
      $response = $this->httpClient->request('GET', $url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());
      return $json;
    }
    catch (RequestException $e) {
      $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e);
    }
  }

  /**
   * Returns a list of the campaigns.
   *
   * @param array $status
   *   An option to filter campaigns by status.
   *
   * @return array
   *   An array of campaigns.
   * @throws \GuzzleHttp\Exception\GuzzleException
   * 
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   */
  public function getCampaigns($status = []) {
    $config = $this->getConfig();
    try {
      $response = $this->httpClient->request('GET', $config['campaigns_url'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());
      $list = [];

      foreach ($json->campaigns as $campaign) {
        if (!empty($status) && in_array($campaign->current_status, $status)) {
          $list[] = $campaign;
        }
        else if (empty($status)) {
          $list[] = $campaign;
        }

      }
      return $list;
    }
    catch (RequestException $e) {
      $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e);
    }
  }

  /**
   * Checks if a contact exists already.
   *
   * @param array $data
   *   Array of data to send. 'email_address' key is required.
   *
   * @return array
   *   Returns a json response that determines if a contact
   *   already exists or was deleted from the list.
   *
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   */
  private function getContact(array $data) {
    $config = $this->getConfig();

    try {
      $response = $this->httpClient->request('GET', $config['contact_url'] . '?email=' . $data['email_address'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());

      if ($json->contacts) {
        return $json;
      }
      else {
        return $this->getDeleted($this->apiUrl . '/contacts?status=deleted&include_count=TRUE', $data['email_address']);
      }
    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e);

      // Return the error to show an error on form submission
      return ['error' => $e];
    }
  }

  /**
   * Gets contact lists from Moosend EMS API.
   * 
   * @param $cached 
   *  Whether to return a cached response or not. 
   *  @see https://www.drupal.org/project/moosend_ems/issues/3282088 and https://docs.moosend.com/developers/api-documentation/en/index-en.html
   *  Cron run perhaps was calling Drupal cached version which may have allowed refresh tokens to expire.
   *
   * @return array
   *   Returns an array of lists that the account has access to.
   *
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   * 
   */
  public function getMailingLists($cached = true) {
    if (version_compare(PHP_VERSION, '8.0', '<')) {
      $this->messenger->addMessage($this->t('The Moosend module is not compatible with your current version of PHP and should be upgraded to latest version or greater than 8.0.'), 'error');
    } else {

      $config = $this->getConfig();

      $cid = 'moosend_ems.lists';
      $cache = ($cached === true ? $this->cache->get($cid) : null);

      if ($cache && $cache->data && count($cache->data) > 0) {
        return $cache->data;
      }
      else {

        if (isset($config['api_key'])) {

          $api_key = $config['api_key'];

          $api_instance = new MailingListsApi();

          $format = "json";
        // string |
          $with_statistics = "";
        // string | Specifies whether to fetch statistics for the subscribers or not. If omitted, results will be returned with statistics by default.
          $short_by = "";
        // string | The name of the campaign property to sort results by. If not specified, results will be sorted by the CreatedOn property
          $sort_method = "asc";
        // string | The method to sort results: ASC for ascending, DESC for descending. If not specified, `ASC` will be assumed

          try {

            $getAllActiveMailingLists = $api_instance->gettingAllActiveMailingLists($format, $api_key, $with_statistics, $short_by, $sort_method);

            if($getAllActiveMailingLists) {

             $context = $getAllActiveMailingLists->getContext();
             if($context) {
               $mailing_lists = $context->getMailingLists();
             }
           }


         } catch (Exception $e) {

         }


         if (!empty($mailing_lists)) {
          foreach ($mailing_lists as $list) {

            $list_data = array(
              'list_id' => $list->getId(),
              'name' => $list->getName(),

            );
            $lists[$list->getId()] = (object)$list_data;

          }

          $this->saveContactLists($lists);
          return $lists;
        }
        else {
          $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
        }

        try {

          $response = $this->httpClient->request('GET', $config['contact_lists_url'], [
            'headers' => [
              'Authorization' => 'Bearer ' . $config['api_key'],
              'cache-control' => 'no-cache',
              'content-type' => 'application/json',
              'accept' => 'application/json',
            ],
          ]);

          $this->updateTokenExpiration();
          $json = json_decode($response->getBody()->getContents());
          $lists = [];

          if ($json->lists) {
            foreach ($json->lists as $list) {
              $lists[$list->list_id] = $list;
            }

            $this->saveContactLists($lists);
            return $lists;
          }
          else {
            $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
          }
        }
        catch (RequestException $e) {
          $this->handleRequestException($e);
          $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
        }
        catch (ClientException $e) {
          $this->handleRequestException($e);
          $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
        }
        catch (\Exception $e) {
          $this->loggerFactory->error($e);
          $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
        }
      }
      else {
        return [];
      }
    }
  }
}

  /**
   * Returns custom fields available
   * 
   * @param $cached 
   *  Whether to return a cached response or not. 
   * 
   * @return mixed
   *   A stdClass of custom fields.
   * 
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.htmlapi_guide/get_custom_fields.html
   */
  public function getCustomFields($cached = true) {
    if (version_compare(PHP_VERSION, '8.0', '<')) {
      $this->messenger->addMessage($this->t('The Moosend module is not compatible with your current version of PHP and should be upgraded to latest version or greater than 8.0.'), 'error');
    } else {
      $config = $this->getConfig();
      $cid = 'moosend_ems.custom_fields';
      $cache = ($cached === true ? $this->cache->get($cid) : null);

      if ($cache && !is_null($cache) && $cache->data && property_exists($cache->data, 'custom_fields')) {
        return $cache->data;
      }
      else {
        if (isset($config['api_key'])) {

         $api_key = $config['api_key'];

         $api_instance = new MailingListsApi();
         $custom_fields_data = new \stdClass();

         $format = "json";
        // string |
         $with_statistics = "";
        // string | Specifies whether to fetch statistics for the subscribers or not. If omitted, results will be returned with statistics by default.
         $short_by = "";
        // string | The name of the campaign property to sort results by. If not specified, results will be sorted by the CreatedOn property
         $sort_method = "asc";
        // string | The method to sort results: ASC for ascending, DESC for descending. If not specified, `ASC` will be assumed


         $getAllActiveMailingLists = $api_instance->gettingAllActiveMailingLists($format, $api_key, $with_statistics, $short_by, $sort_method);
         $custom_fields = array();

         if($getAllActiveMailingLists) {

           $context = $getAllActiveMailingLists->getContext();
           if($context) {
             $mailing_lists = $context->getMailingLists();

             $AddingSubscribersRequest =  new AddingSubscribersRequest();


             foreach ($mailing_lists as $list) {
              $mailing_list_id = $list->getId();
              $mailing_list_label = $list->getName();

              $with_statistics = "";
            // string | Specifies whether to fetch statistics for the subscribers or not. If omitted, results will be returned with statistics by default.

              $result = $api_instance->gettingMailingListDetails($format, $mailing_list_id, $api_key, $with_statistics);

              $context = $result->getContext();
              $custom_fields_definitions = $context->getCustomFieldsDefinition();

              if(!empty($custom_fields_definitions)) {
                foreach ($custom_fields_definitions as $key => $custom_fields_definition) {
                  $id = $custom_fields_definition->getId();
                  $label = $custom_fields_definition->getName();
                  $type = $custom_fields_definition->getType();
                  $is_required = $custom_fields_definition->getIsRequired();

                  $data_value = new \stdClass();
                  switch ($type) {
                    case '1':
                    $type = 'number';
                    break;

                    case '2':
                    $type = 'date';
                    break;

                    case '3':
                    $type = 'select';
                    $xml = $custom_fields_definition->getContext();

                    $context_array = json_decode(json_encode((array)simplexml_load_string($xml)),true);

                    $options = array();
                    if(is_array($context_array) && isset($context_array['item']) && !empty($context_array['item'])) {
                      foreach ($context_array['item'] as $key => $value) {
                        $options[$value['name']] = $value['value'];
                      }
                    }
                    $data_value->options = $options;

                    break;

                    case '4':
                    $type = 'checkbox';

                    break;

                    default:
                    $type = 'string';
                    break;
                  }

                  $data_value->name = $label;
                  $data_value->label = $label;
                  $data_value->type = $type;
                  $data_value->custom_field_id = $id;
                  $data_value->mailing_list_id = $mailing_list_id;
                  $data_value->mailing_list_label = $mailing_list_label;
                  $data_value->is_required = $is_required;

            // if(!in_array($data_value, $custom_fields)) {
                  $custom_fields[] = $data_value;
            // }
                }
              }
            }
          }
        }
        $custom_fields_data->custom_fields = $custom_fields;

        $url = $this->apiUrl . '/contact_custom_fields';

        try {

          $this->cache->set($cid, $custom_fields_data);

          return $custom_fields_data;
        }
        catch (RequestException $e) {
          $this->handleRequestException($e);
        }
        catch (ClientException $e) {
          $this->handleRequestException($e);
        }
        catch (\Exception $e) {
          $this->loggerFactory->error($e);
        }
      }
    }
  }
}

  /**
   * Checks if a contact is deleted from a list.
   *
   * This loops through all the deleted contacts of a
   * list and returns if there is a match to the email address.
   *
   * @param string $endpoint
   *   The endpoint to check. @see $this->getContact()
   * @param string $email
   *   The email address we're looking for.
   *
   * @return array
   *   Returns an array of a matched deleted contact.
   *
   * @see https://community.moosendems.com/t5/Developer-Support-ask-questions/API-v-3-409-conflict-on-POST-create-a-Contact-User-doesn-t/td-p/327518
   */
  private function getDeleted($endpoint, $email) {
    $config = $this->getConfig();

    $deleted = $this->httpClient->request('GET', $endpoint, [
      'headers' => [
        'Authorization' => 'Bearer ' . $config['api_key'],
        'cache-control' => 'no-cache',
        'content-type' => 'application/json',
        'accept' => 'application/json',
      ],
    ]);

    $deleted = json_decode($deleted->getBody()->getContents());
    $match = NULL;

    if (count($deleted->contacts)) {
      foreach ($deleted->contacts as $value) {
        if ($value->email_address->address === $email) {
          $match = $value;
        }
      }
    }

    if (!$match &&  property_exists($deleted, '_links') && property_exists($deleted->_links, 'next') && property_exists($deleted->_links->next, 'href')) {
      $match = $this->getDeleted('https://api.cc.email' . $deleted->_links->next->href, $email);
    }

    return $match;
  }

  /**
   * Get Enabled Contact Lists 
   *
   * @param boolean $cached
   * @return array $lists of enabled lists
   * 
   * @see /Drupal/Form/MoosendEmsLists.php
   */
  public function getEnabledMailingLists($cached = true) {
    $lists = $this->getMailingLists($cached);

    return $lists;
  }

  /**
   * Get the permanent link of a campaign.
   *
   * @param string $id
   *   The campaign id.
   *
   * @return null|string
   *   The URL of the campaign's permanent link.
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getPermaLinkFromCampaign(string $id) {
    $url = NULL;
    if (!$id) {
      return NULL;
    }
    $campaign = $this->getCampain($id);
    foreach ($campaign->campaign_activities as $activity) {
      if ($activity->role != 'permalink') {
        continue;
      }
      $act = $this->getCampaignActivity($activity->campaign_activity_id);
      if ($act) {
        return $act->permalink_url;
      }
    }
    return NULL;
  }

  /**
   * Handles an error
   *
   * @param [object] $error
   * @return [array] $return
   */
  protected function handleRequestException(object $e) {
    $response = $e->getResponse();
    $error = is_null($response) ? FALSE : json_decode($response->getBody());

    $message = 'RequestException: ';

    $errorInfo = [];

    if ($error && is_object($error)) {
      if (property_exists($error, 'error')) {
        $errorInfo[] = $error->error;
      }

      if (property_exists($error, 'message')) {
        $errorInfo[] = $error->message;
      }

      if (property_exists($error, 'error_description')) {
        $errorInfo[] = $error->error_description;
      }

      if (property_exists($error, 'errorCode')) {
        $errorInfo[] = $error->errorSummary;
      }

      if (property_exists($error, 'errorCode')) {
        $errorInfo[] = $error->errorCode;
      }
    }

    $message .= implode(', ', $errorInfo);

    $this->loggerFactory->error($message);

    // Return the error to show an error on form submission
    return ['error' => $message];
  }

  /**
   * Handles API response for adding a contact.
   *
   * @param object $response
   *   The json_decoded json response.
   * @param string $method
   *   The name of the method that the response came from.
   *
   * @return array
   *   Returns an array that includes the method name and
   *   the statuscode except if it is coming from getContact method.
   *   Then it returns an array of the contact that matches.
   */
  private function handleResponse($response, $method) {

    if ($response->getError() != 'null' || ($response->getError() != 'null' && $method === 'createContact')) {
      $statuscode = $response->getCode();
      $context = $response->getContext();

      $this->loggerFactory->info('@method has been executed successfully.', ['@method' => $method]);

      $this->updateTokenExpiration();

      if ($method === 'getContact') {
        return $context;
      }

      return [
        'method' => $method,
        'response' => $statuscode,
      ];
    }
    else {
      $statuscode = $response->getCode();
      $responsecode = $response->getError();

      $this->loggerFactory->error('Call to @method resulted in @status response. @responsecode', [
        '@method' => $method,
        '@status' => $statuscode,
        '@responsecode' => $responsecode,
      ]);

      return [
        'error' => 'There was a problem signing up. Please try again later.',
      ];
    }
  }

  /**
   * Submits a contact to the API. 
   * Used to be used on CostantContactBlockForm but using $this->submitContactForm instead.
   * Determine if contact needs to be updated or created.
   *
   * @param array $data
   *   Data to create/update a contact.
   *   Requires a 'email_address' key.
   *   But can also accept 'name' and 'mobile' key.
   * @param array $listIDs
   *   An array of list UUIDs to post the contact to.
   *
   * @return array
   *   Returns an error if there is an error.
   *   Otherwise it sends the info to other methods.
   *
   * @see $this->updateContact
   * @see $this->putContact
   * @see $this->createContact
   */
  public function postContact(array $data = [], $listIDs = []) {
    $config = $this->getConfig();
    $enabled = $this->config->get('moosend_ems.enabled_lists')->getRawData();

    if (!$config['api_key'] || !$config['client_secret'] || !$config['api_key'] || !$data) {
      $msg = 'Missing credentials for postContact';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    if (!$listIDs || count($listIDs) === 0) {
      $msg = 'A listID is required.';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    foreach ($listIDs as $listID) {
      if (!isset($enabled[$listID]) || $enabled[$listID] !== 1) {
        $msg = 'The listID provided does not exist or is not enabled.';

        $this->loggerFactory->error($msg);

        return [
          'error' => $msg,
        ];
      }
    }

    if (!isset($data['email_address'])) {
      $msg = 'An email address is required';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    // Check if contact already exists.
    $exists = (array) $this->getContact($data);

    // If yes, updateContact.
    // If no, createContact.
    // If previous deleted, putContact.
    if (isset($exists['contacts']) && count($exists['contacts']) > 0) {
      $this->updateContact($data, $exists['contacts'][0], $listIDs);
    }
    elseif ($exists && isset($exists['deleted_at'])) {
      $this->putContact($exists, $data, $listIDs);
    }
    else {
      $this->createContact($data, $listIDs);
    }
  }

  /**
   * Updates a contact if it already exists and has been deleted.
   *
   * @param array $contact
   *   The response from $this->getDeleted.
   * @param array $data
   *   The $data provided originally. @see $this->postContact.
   * @param array $listIDs
   *   The list IDs we want to add contact to.
   *
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   * @see $this->getDeleted
   *
   * @TODO perhaps combine this with updateContact. The difference is that $contact is
   * an array here and an object in updateContact.
   */
  private function putContact(array $contact, array $data, $listIDs) {
    $config = $this->getConfig();

    $body = (object) $contact;

    $body = $this->buildResponseBody($data, $body);

    $body->email_address->permission_to_send = 'implicit';
    // To resubscribe a contact after an unsubscribe update_source must equal Contact. 
    // @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
    $body->update_source = 'Contact';
    $body->list_memberships = $listIDs;

    $this->moduleHandler->invokeAll('moosend_ems_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('moosend_ems_contact_update_data_alter', [$data, &$body]);

    try {
      $response = $this->httpClient->request('PUT', $config['contact_url'] . '/' . $contact['contact_id'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
        'body' => json_encode($body),
      ]);

      $this->handleResponse($response, 'putContact');

    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      return $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e);

      // Return the error to show an error on form submission
      return ['error' => $e];
    }
  }

  /**
   * Makes authenticated request to Moosend EMS to refresh tokens.
   *
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   */
  public function refreshToken($updateLists = true) {
    $config = $this->getConfig();

    if (!$config['api_key']) {
      return FALSE;
    }

    // @TODO - Fix for pkce flow. 
    // @see https://www.drupal.org/project/moosend_ems/issues/3285446
    // @see https://docs.moosend.com/developers/api-documentation/en/index-en.htmlapi_guide/pkce_flow.html
    try {
      $response = $this->httpClient->request('POST', $this->tokenUrl, [
        'headers' => [
          'Authorization' => 'Basic ' . base64_encode($config['api_key'] . ':' . $config['client_secret']),
        ],
        'form_params' => [
          'refresh_token' => $config['refresh_token'],
          'grant_type' => 'refresh_token',
        ],
      ]);

      $json = json_decode($response->getBody()->getContents());

      $this->saveTokens($json);

      if ($updateLists === true) {
        $this->getMailingLists(false);
      }
    }
    catch (RequestException $e) {
      return $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      dpm($e);
      $this->loggerFactory->error($e);

      // Return the error to show an error on form submission
      return ['error' => $e];
    }
  }

  /**
   * Saves available contact lists to a cache.
   *
   * @param array $data
   *   An array of lists and list UUIDs from $this->getMailingLists.
   */
  public function saveContactLists(array $data) {
    $cid = 'moosend_ems.lists';
    $enabled = $this->config->get('moosend_ems.enabled_lists')->getRawData();
    
    // Add an enabled flag to the list data.
    foreach ($data as $key => $value) {
      $data[$key]->enabled = (isset($enabled[$key]) && $enabled[$key] === 1);
      $data[$key]->cached_on = strtotime('now'); 
    }

    $this->cache->set($cid, $data);
  }

  /**
   * Saves access and refresh tokens to our config database.
   *
   * @param object $data
   *   Data object of data to save the token.
   *
   * @see $this->refreshToken
   */
  private function saveTokens($data) {
    if ($data && property_exists($data, 'api_key') && property_exists($data, 'refresh_token')) {
      $tokens = $this->config->getEditable('moosend_ems.tokens');
      $tokens->clear('moosend_ems.tokens');
      $tokens->set('api_key', $data->api_key);
      $tokens->set('refresh_token', $data->refresh_token);
      $tokens->set('timestamp', strtotime('now'));

      if ($data->expires_in < $tokens->get('expires')) {
        $tokens->set('expires', $data->expires_in);
      }

      $tokens->save();

      $this->loggerFactory->info('New tokens saved at ' . date('Y-m-d h:ia', strtotime('now')));
    }
    else {
      $this->loggerFactory->error('There was an error saving tokens');
    }
  }

  /**
   * Submission of contact form.
   * Replaces $this->postContact as of v. 2.0.9.
   *
   * @param array $data
   *   Data to create/update a contact.
   *   Requires a 'email_address' key.
   *   But can also accept 'name' and 'mobile' key.
   * @param array $listIDs
   *   An array of list UUIDs to post the contact to.
   *
   * @return array
   *   Returns an error if there is an error.
   *   Otherwise it sends the info to other methods.
   *
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   */
  public function submitContactForm(array $data = [], $listIDs = []) {
    $config = $this->getConfig();


    $enabled = $this->config->get('moosend_ems.enabled_lists')->getRawData();

    if (!$config['api_key'] || !$data) {
      $msg = 'Missing credentials for postContact';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    if($listIDs && !is_array($listIDs)) {
      $listIDs = [$listIDs];
    }

    if (!$listIDs || count($listIDs) === 0) {

      $msg = 'A listID is required.';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    foreach ($listIDs as $listID) {
      if (!isset($enabled[$listID]) || $enabled[$listID] !== 1) {
        $msg = 'The listID provided does not exist or is not enabled.';

        $this->loggerFactory->error($msg);

        return [
          'error' => $msg,
        ];
      }
    }

    if (!isset($data['email_address'])) {
      $msg = 'An email address is required';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    $body = (object)[];

    $body = $this->buildResponseBody($data, $body);

    // Add required fields.
    $body->email_address = $data['email_address'];
    $body->list_memberships = $listIDs;

    $this->moduleHandler->invokeAll('moosend_ems_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('moosend_ems_contact_form_submission_alter', [$data, &$body]);

    try {

      $api_instance = new SubscribersApi();
      $email_address = $data['email_address'];
      $name = $data['name']??'';
      $mobile = $data['mobile']??'';
      $custom_fields_data = $data['custom_fields']??[];
      $custom_fields = [];
      $format = "json";
      $apikey = $config['api_key'];

      if(!empty($custom_fields_data)) {
        foreach ($custom_fields_data as $field_key => $field_value) {
          $custom_fields[] = $field_key.'='.$field_value;
        }
      }

      foreach ($listIDs as $key => $mailing_list_id) {

        // get subscriber by email
        // $result = $api_instance->getSubscriberByEmailAddress($format, $apikey, $mailing_list_id, 'qypynac@mailinator.com');

        $subscriber_body = new AddingSubscribersRequest();

        $subscriber_body->setEmail($email_address);
        $subscriber_body->setName($name);
        $subscriber_body->setCustomFields($custom_fields);

        $response = $api_instance->addingSubscribers($format, $mailing_list_id, $apikey, $subscriber_body);

        $this->handleResponse($response, 'submitContactForm');

      }
    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e);

      // Return the error to show an error on form submission
      return ['error' => $e];
    }
  }


  /**
   *  Unsubscribes a contact from all lists.
   *
   * @param array $contact
   *   The response from $this->getDeleted.
   * @param array $data
   *   The $data provided
   *
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   *
   */
  public function unsubscribeContact(array $data) {
    $config = $this->getConfig();
    // Check if contact already exists.
    $exists = (array) $this->getContact($data);
    $body = null;

    $email_address = $data['email_address'];
    $mailing_list_id = $data['mailing_list_id']??'';

    $this->moduleHandler->invokeAll('moosend_ems_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('moosend_ems_contact_unsubscribe_data_alter', [$data, &$body]);

    try {
      if($mailing_list_id != '') {

        $api_instance = new SubscribersApi();
        $format = "json";
        // string |
        $apikey = $config['api_key'];
        $body = new UnsubscribingSubscribersFromMailingListRequest();
        $body->setEmail($email_address);

        $response = $api_instance->unsubscribingSubscribersFromMailingList($format, $apikey, $mailing_list_id, $body);

        $this->handleResponse($response, 'unsubscribeContact');
      }
    }
    catch (RequestException $e) {
        // Return the error to show an error on form submission
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      return $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e);

        // Return the error to show an error on form submission
      return ['error' => $e];
    }
    // }

  }

  /**
   * Updates a contact if it already exists on a list.
   *
   * @param array $data
   *   Provided data to update the contact with.
   * @param object $contact
   *   Contact match to provided data.
   * @param array $listIDs
   *   An array of list UUIDs that the contact is being updated on.
   *
   * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
   */
  private function updateContact(array $data, $contact, $listIDs) {
    $config = $this->getConfig();

    if ($contact && property_exists($contact, 'contact_id')) {

      $body = $contact;
      $body = $this->buildResponseBody($data, $body);

      // Add required fields
      $body->email_address->address = $data['email_address'];
      $body->email_address->permission_to_send = 'implicit';
      $body->update_source = 'Contact';
      $body->list_memberships = $listIDs;

      $this->moduleHandler->invokeAll('moosend_ems_contact_data_alter', [$data, &$body]);
      $this->moduleHandler->invokeAll('moosend_ems_contact_update_data_alter', [$data, &$body]);

      try {
        $response = $this->httpClient->request('PUT', $config['contact_url'] . '/' . $contact->contact_id, [
          'headers' => [
            'Authorization' => 'Bearer ' . $config['api_key'],
            'cache-control' => 'no-cache',
            'content-type' => 'application/json',
            'accept' => 'application/json',
          ],
          'body' => json_encode($body),
        ]);

        return $this->handleResponse($response, 'updateContact');

      }
      catch (RequestException $e) {
        // Return the error to show an error on form submission
        return $this->handleRequestException($e);
      }
      catch (ClientException $e) {
        $this->handleRequestException($e);
      }
      catch (\Exception $e) {
        $this->loggerFactory->error($e);

        // Return the error to show an error on form submission
        return ['error' => $e];
      }
    }
    else {
      $this->loggerFactory->error('error: No contact id provided for updateContact method');
      return ['error: No contact id provided'];
    }
  }

  protected function updateTokenExpiration() {
    $tokens = $this->config->getEditable('moosend_ems.tokens');
    $tokens->set('expires', strtotime('now +2 hours'));
    $tokens->save();
  }

}
