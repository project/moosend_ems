<?php

namespace Drupal\moosend_ems\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\moosend_ems\Service\MoosendEms;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines CustomFieldsController class.
 */
class CustomFieldsController extends ControllerBase {

  /**
   * Constructor function.
   *
   * @param \Drupal\moosend_ems\Service\MoosendEms $moosendEms
   *   Moosend ems service.
   */
  public function __construct(MoosendEms $moosendEms) {
    $this->moosendEms = $moosendEms;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('moosend_ems')
    );
  }

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    $fields = $this->moosendEms->getCustomFields(false);
    $header = ['Custom Field Name', 'Field Type', 'Custom Field ID'];
    $rows = [];

    if ($fields && is_array($fields->custom_fields) && count($fields->custom_fields) > 0)  {
      $markup = $this->t('Custom fields available for your account:') . '<ul>';
      foreach ($fields->custom_fields as $field) {
        $rows[] = [
          $this->t($field->label),
          $this->t($field->type),
          [
            'data' => [
              '#markup' => '<code>' . $field->custom_field_id . '</code>'
            ]
          ]
        ];
      }
      
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('There are no custom fields found.')
    ];
  }

}