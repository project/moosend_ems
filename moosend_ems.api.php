<?php

/**
 * @file
 * Hooks provided by the Moosend EMS module.
 */

use Drupal\webform\Plugin\WebformHandlerInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 *
 * For example if you would like to send custom fields.
 *
 * @see https://docs.moosend.com/developers/api-documentation/en/index-en.html
 *
 */

/**
 * hook_moosend_ems_contact_data_alter
 *
 * Alters the contact data that is sent to Moosend EMS.
 * This hook is triggered on both create and update of contacts.
 *
 * @param array $data - The data received from a moosend ems form.
 * @param object $body - The content of the request that is sent to the Moosend EMS API
 * @return void
 */
function hook_moosend_ems_contact_data_alter(array $data, object &$body) {
  // Add custom field values
  // Set a value directly.
  $body->custom_fields[] = (object) [
    'custom_field_id' => '00000000-0000-0000-0000-0000000000',  // The UUID of the custom field
    'value' => 'Some Value', // A value.
  ];

  // Add custom field values
  // Set based on $data value.
  $body->custom_fields[] = (object) [
    'custom_field_id' => '00000000-0000-0000-0000-0000000000',  // The UUID of the custom field
    'value' => $data['company'], // A value.
  ];

}

/**
 * Alter mergevars before they are sent to Moosend EMS.
 *
 * @param array $mergevars
 *   The current mergevars.
 * @param WebformSubmissionInterface $submission
 *   The webform submission entity used to populate the mergevars.
 * @param WebformHandlerInterface $handler
 *   The webform submission handler used to populate the mergevars.
 *
 * @ingroup webform_moosend_ems
 */
function hook_moosend_ems_lists_mergevars_alter(&$mergevars, WebformSubmissionInterface $submission, WebformHandlerInterface $handler) {

}
